const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync');

gulp.task('image', function () {
    gulp.src('./src/img/*.+(png|svg)')
        .pipe(image())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            interlaced: true
        }))
        .pipe(gulp.dest('./build/img'));
});

gulp.task('fonts', function () {
    gulp.src('./src/font//**/*{ttf,woff,woff2,svg,eot,css}')
        .pipe(gulp.dest('./build/fonts'));

});


gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
});

gulp.task("sass", function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./build/styles'));
});


gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        }
    })
});

gulp.task('watch', ['sass', 'html', 'image', 'browserSync', 'fonts'], function () {
    gulp.watch('./src/styles**/*.scss', ['sass']);
    gulp.watch('./src/assets/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/font//**/*{ttf,woff,woff2,svg,eot,css}', ['fonts']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'image']);